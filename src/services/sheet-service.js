
export default class SheetService {

    static async retriveAllSheet() {
        try {
            const token = window.localStorage.getItem('token');
            const response = await fetch(
                `${process.env.REACT_APP_SERVER_ADDRESS}/sheets`,
                {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    }
                }
            );
            const result = await response.json();
            const { data } = result;
            return data;

        } catch (e) {
            console.log(e);
            return [];
        }
    }

    static async retriveOneSheet(id) {
        try {
            const token = window.localStorage.getItem('token');
            const response = await fetch(
                `${process.env.REACT_APP_SERVER_ADDRESS}/sheets/` + id,
                {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    },
                }
            );
            const result = await response.json();
            const { data } = result;
            return data;

        } catch (e) {
            console.log(e);
            return [];
        }
    }

    static async deleteSheet(id) {
        const token = window.localStorage.getItem('token');
        const response = await fetch(
            `${process.env.REACT_APP_SERVER_ADDRESS}/sheets/` + id,
        {
            method: 'DELETE',
            headers: {
                'Authorization': 'Bearer ' + token
            },
        });
        const result = await response.json();
            const { data } = result;
            return data;
    }

    static async addNewSheet(sheetData) {
        const token = window.localStorage.getItem('token');
        try{
            const response = await fetch(
                `${process.env.REACT_APP_SERVER_ADDRESS}/sheets`,
            {
                method: 'POST',
                body: JSON.stringify(sheetData),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            });
            const result = await response.json();
            const  { data } = result;

            return result;
        } catch(e) {
            console.log(e);
        }
    }
}