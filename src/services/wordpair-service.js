export default class WordPairService {

    static async loadAllWordpairs() {
        const token = window.localStorage.getItem('token');
        try {
            const response = await fetch(
                `${process.env.REACT_APP_SERVER_ADDRESS}/words/`,
                {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    }
                });
                const result = await response.json();
                
                const [ data ] = result;
                return data; 
            
        } catch(err) {
            console.log(err);
        }
    }

    static async createWordPair(userData) {
        const token = window.localStorage.getItem('token');
        try{
            const response = await fetch(
                `${process.env.REACT_APP_SERVER_ADDRESS}/words/sheets/` + userData.id,
                {
                    method: 'POST',
                    body: JSON.stringify(userData),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token,
                    },
                }
            );
            const result = await response.json();
            const {data} = result;
            
            return result;

        } catch (e) {
            console.log(e);
            return {
                "success": false,
                "error": "Unexpected server error!"
            }
        }
    }

    static async getWordPair() {
        try {
            const response = await fetch(
            `${process.env.REACT_APP_SERVER_ADDRESS}/labels`,
            {
                method: 'GET',

            }
            );
            const result = await response.json();
            const { data } = result;
            return result;
        } catch(e) {
            console.log(e);
            return {
                "success": false,
                "error": "Unexpected server error!"
            }
        }
    }

    static async deleteWordpair(id) {
        try {
            const token = window.localStorage.getItem('token');
            const response = await fetch(
                `${process.env.REACT_APP_SERVER_ADDRESS}/words/` + id,
                {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token,
                    }
                }
            );
            const result = await response.json();

            return result;

        } catch(err) {
            console.log(err);
        }
    }

    static async updateWordpair(userData) {
        try {
            const token = window.localStorage.getItem('token');
            const response = await fetch(
                `${process.env.REACT_APP_SERVER_ADDRESS}/words/` + userData.id,
                {
                    method: 'PUT',
                    body: JSON.stringify(userData),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    }
                }
            );
            const result = await response.json();
            return result;
        } catch(err) {
            return err;
        }
    }
}