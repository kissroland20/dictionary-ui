export default class UserService {

    static logout() {
        window.localStorage.clear();
    }

    static isUserLoggedIn() {
        const token = window.localStorage.getItem('token');
        
        return !!token;
    }

    static async loginUser(userData) {
        if (process.env.REACT_APP_LOCAL_ENV) {
            const p = new Promise((resolve, reject) => {
                resolve({
                    async json() {
                        return {
                            "data": {
                                "username": "czirjak@gmail.com",
                                "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjemlyamFrQGdtYWlsLmNvbSIsInJvbGVzIjoiUk9MRV9VU0VSIiwiaWF0IjoxNjI1Mjk4NTg2LCJleHAiOjE2MjUzMTY1ODZ9.J9KvwD6ItPE9-K5wZbUjLH4vmO5eoxvRPx87YhkoGh7GTBasRZrmlFL8xh3_YYwKWL1VVKpbz306TYkunA_4sA",
                                "role": "ROLE_USER"
                            },
                            "success": true,
                            "error": ""
                        }
                    }
                })
            });

            const response = await p;
            const result = await response.json();
            const { data } = result;

            window.localStorage.setItem('token', data.token);
            window.localStorage.setItem('user-role', data.role);

            return result;
        } else {
            try {
                const response = await fetch(
                    `${process.env.REACT_APP_SERVER_ADDRESS}/login`,
                    {
                        method: 'POST',
                        body: JSON.stringify(userData),
                        headers: {
                            'Content-Type': 'application/json'
                        },
                    }
                );
                const result = await response.json();
                const { data } = result;

                window.localStorage.setItem('token', data.token);
                window.localStorage.setItem('user-role', data.role);

                return result;
            } catch (e) {
                console.log(e);
                return {
                    "success": false,
                    "error": "Unexpected server error!"
                }
            }

        }
    }

    static async registerUser(userData) {
        if (process.env.REACT_APP_LOCAL_ENV) {
            const p = new Promise((resolve, reject) => {
                resolve({
                    async json() {
                        return {
                            "data": {
                                "id": 1,
                                "username": "czirjak@gmail.com",
                                "password": "z1PmimctbUxWotgt5lhKow==",
                                "role": "ROLE_USER",
                                "active": true
                            },
                            "success": true,
                            "error": "Username is already in used!"
                        }
                    }
                })
            });

            const response = await p;
            return response.json();
        } else {
              //const token = window.localStorage.getItem('token');
            try {
                const response = await fetch(
                    `${process.env.REACT_APP_SERVER_ADDRESS}/registration`,
                    {
                        method: 'POST',
                        body: JSON.stringify(userData),
                        headers: {
                            'Content-Type': 'application/json',
                           // 'Authorization': `Bearer ${token}`
                        },
                    }
                );
                return await response.json();
            } catch (e) {
                console.log(e);
                return {
                    "success": false,
                    "error": "Unexpected server error!"
                }
            }

        }
    }

    static async deleteUserById() {

    }

    static async changePassword(data) {
        try {
            const token = window.localStorage.getItem('token');
            const response = await fetch(
                `${process.env.REACT_APP_SERVER_ADDRESS}/password/?action=change`,
                {
                    method: 'POST',
                    body: JSON.stringify(data),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token,
                    }
                }
            );
            const result = await response.json();
            return result;

        } catch(err) {
            console.log(err);
        }        
    }
}