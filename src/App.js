import './App.css';
import PrimarySearchAppBar from './components/PrimarySearchAppBar';
import Home from './components/Home';
import Login from './components/Login';
import RegisterForm from './components/RegisterForm';
import MyDict from './components/MyDict';
import Practice from './components/Practice';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Forbidden from './components/Forbidden';
import WordpairComponent from './components/WordpairComponent';
import Profile from './components/Profile';
import React, { useReducer } from 'react';
import GuardedRoute from './components/GuardedRoute';

function reducer(state, action) {
  console.log(state);
  switch (action.type) {
    case 'changeToLight':
      return {
        ...state,
        theme: 'light',
        numberOfChanges: state.numberOfChanges + 1,
      }
    case 'changeToDark':
      return {
        ...state,
        theme: 'dark',
        numberOfChanges: state.numberOfChanges + 1,
      }
    case 'changeTheme':
      return {
        ...state,
        theme: action.payload,

      }
    default:
      throw new Error();
  }
}

export const themes = {
  light: {
    background: "#FFFFFF",
    foreground: "#3F51B5",
  },
  dark: {
    background: "gray",
    foreground: "#FFFFFF",
  }
}

export const StateContext = React.createContext();
export const DispatchContext = React.createContext();

export default function App() {
  const [state, dispatch] = useReducer(reducer, {
    theme: 'light',
    numberOfChanges: 0,
  });

  const routes = [
    {
      path: '/mydict/:id',
      component: WordpairComponent,
      key: 2,
    },
    {
      path: '/mydict',
      component: MyDict,
      key: 1,
    },
    {
      path: '/profile',
      component: Profile,
      key: 3,
    },
    {
      path: '/practice',
      component: Practice,
      key: 4,
    },
  ];


  return (
    <Router>
      <div>
        <DispatchContext.Provider value={dispatch} >
          <StateContext.Provider value={themes[state.theme]} >
            <PrimarySearchAppBar />
            {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
            <Switch>
              <Route path="/login">
                <Login />
              </Route>
              <Route path="/forbidden">
                <Forbidden />
              </Route>
              <Route path="/register">
                <RegisterForm />
              </Route>

              {
            routes.map(route =>
              <GuardedRoute key={route.key} path={route.path} component={route.component} />
            )
          }

               {/* <GuardedRoute path="/mydict/"> 
                <Route path="/mydict/" exact>
                <MyDict />
                </Route>
                </GuardedRoute>
                <Route path="/mydict/:id">
                <WordpairComponent />
                </Route>
                <Route path="/profile">
                <Profile />
                </Route>
                <Route path="/practice">
                <Practice />
              </Route> */}
              <Route path="/">
                <Home />
              </Route>
            </Switch>
          </StateContext.Provider>
        </DispatchContext.Provider>
      </div>
    </Router>
  );
}