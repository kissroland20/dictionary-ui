import { Button, CircularProgress, FormControl, TextField } from "@material-ui/core";
import { useState } from "react";
import { useLocation } from "react-router-dom";
import classes from './style/AddWordPair.module.css';
import WordPairService from "../services/wordpair-service";
import SheetService from "../services/sheet-service";

export default function AddWordPair(props) {
    const location = useLocation();
    const [isPending, setIsSpenging] = useState(false);
    const [userData, setUserData] = useState({
        leftValue: '',
        rightValue: '',
        id: ''
    });

    function handleChange(event) {
        const idFromURL = +(location.pathname.match(/[0-9]+/g));
        const { name, value } = event.target;
        setUserData({
            ...userData,
            [name]: value, // [event.target.name]: event.target.value - MIért nem jó???
            id: idFromURL
        });
    }

    async function handleClick() {
        setIsSpenging(true);
        const idFromURL = +(location.pathname.match(/[0-9]+/g));
        await WordPairService.createWordPair(userData);
        const fetchedWordpairs = await SheetService.retriveOneSheet(idFromURL);
        props.setWordPairs(fetchedWordpairs.wordPairs);
        setIsSpenging(false);
    }

    return (
        <>
            <FormControl >
            <div className={classes.addWordFormControl}>
                <TextField 
                    className={classes.addWordTextField} 
                    multiline label="Word" 
                    name="leftValue" 
                    onChange={handleChange}
                >
                </TextField>
                <TextField 
                    className={classes.addWordTextField} 
                    multiline label="Translation" 
                    name="rightValue" 
                    onChange={handleChange}
                >
                </TextField>
                {
                    isPending
                    ?
                    <CircularProgress />
                    :
                <Button
                    variant="contained"
                    size="small"
                    onClick={handleClick}
                >
                    Save
                </Button>
                }
            </div>
            </FormControl>
        </>
    );
}