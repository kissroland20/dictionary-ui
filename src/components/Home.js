import { makeStyles } from "@material-ui/core";
import { useContext } from "react";
import { StateContext } from "../App";


const useStyles = makeStyles({
    home: {
        textAlign: 'center',
    }
});

export default function Home (){
    const classes = useStyles();
    const theme = useContext(StateContext);

    return (
        <div 
            className={classes.home} 
            style={{
                color: theme.foreground,
                background: theme.background
            }}
            >
            <h1>Üdvözöllek a DictioWorld alkalmazásban!</h1>
        </div>
    )
}