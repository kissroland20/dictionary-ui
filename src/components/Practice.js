import { Button, MenuItem, Select } from '@material-ui/core';
import { useEffect, useState } from 'react';
import classes from './style/practice.module.css';
import SheetService from '../services/sheet-service';
import LeftValue from './LeftValue';
import RightValue from './RightValue';

export default function Practice() {
    const [dictionaryTitles, setDictionaryTitles] = useState([]);
    const [words, setWords] = useState([]);
    const [isShowAnswerClicked, setShowAnswerClicked] = useState(false);
    const [selectedWordPair, setSelectedWordPair] =  useState({});

    useEffect(() => {
        async function loadDictionaries() {
            const dictTitles = await SheetService.retriveAllSheet();
            setDictionaryTitles(dictTitles);
        }
        loadDictionaries();
    }, []);

    useEffect(() => {
      console.log("words változott...");
      const randomIndex = getRandomNumber(0, words.length);
      console.log(words[randomIndex]);
      if(words[randomIndex]) {
        setSelectedWordPair(words[randomIndex]);
      }
    }, [words]);
    
    function getRandomNumber(min, max) { 
        return Math.floor(Math.random() * (max - min) + min);
    } 

    function showAnswerButton() {
        setShowAnswerClicked(true);
    }

    function submitAnswerButton() {
        setShowAnswerClicked(false);
        const randomIndex = getRandomNumber(0, words.length);
        setSelectedWordPair(words[randomIndex]);
    }

    return (
        <div className={classes.box}>
            <h2>Chose a dictionary: </h2>
            <Select

            onChange={async (e) => {
                console.log(e.target.value);
                const wordsFromDictionary = await SheetService.retriveOneSheet(e.target.value);
                setWords(wordsFromDictionary.wordPairs);
            }}
            >
                {
                    dictionaryTitles.map((option) => (
                        <MenuItem
                            key={option.id}
                            id={option.id}
                            value={option.id}                     
                        >
                            {option.name}

                        </MenuItem>
                    ))}
            </Select>
            <div className={classes.wordsButtonsBox}>
                <div className={classes.wordTextField}>
                    {
                        words.length !== 0
                            ?
                            <>
                                <LeftValue show={isShowAnswerClicked} value={selectedWordPair.leftValue} />
                                <RightValue show={isShowAnswerClicked} value={selectedWordPair.rightValue} />
                            </>
                            :
                            'Válassz szótárat! Lehet, hogy ide még nem tölttél szavakat:('
                    }
                </div>
                {
                    !isShowAnswerClicked
                        ?
                        <Button
                            variant="outlined"
                            onClick={showAnswerButton}>
                            Show Answer
                        </Button>
                        :
                        <div className={classes.buttons}>
                            <Button
                                className={classes.buttonStyle}
                                variant="outlined"
                                onClick={submitAnswerButton}>
                                No clue
                            </Button>
                            <Button
                                className={classes.buttonStyle}
                                variant="outlined"
                                onClick={submitAnswerButton}>
                                Easy
                            </Button>
                        </div>
                }
            </div>
        </div>
    )
}
