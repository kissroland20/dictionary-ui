import { TextField } from "@material-ui/core";
import classes from './style/practice.module.css';

export default function LeftValue(props) {
    return (
        <>
            <TextField
                className={classes.wordStyle}
                id="outlined-secondary-1"
                variant="outlined"
                color="secondary"
                disabled
                value={props.value}          
            />
        </>
    )
}