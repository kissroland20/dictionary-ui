import { useEffect, useState } from "react";
import AddWordPair from "./AddWordPair";
import WordPair from "./WordPair";
import styles from "./style/wordpair.module.css";
import SheetService from "../services/sheet-service";
import { useLocation } from "react-router-dom";

export default function WordpairComponent() {
    const [wordPairs, setWordPairs] = useState([]);
    const [dictionary, setDictionary] = useState({
        name: '',
        description: '',
    });
    const location = useLocation();

    useEffect(() => {
        async function loadWordpairs() {
            const idFromURL = +(location.pathname.match(/[0-9]+/g));
            const fetchedWordpairs = await SheetService.retriveOneSheet(idFromURL);
            console.log(fetchedWordpairs);
            setDictionary({
                name: fetchedWordpairs.name,
                description: fetchedWordpairs.description
            })
            setWordPairs(fetchedWordpairs.wordPairs);
        }
        loadWordpairs();
    }, []);

    return (
        <div className={styles.wordpairs}>
            <div className={styles.titlePart}>
                <h2>
                    {dictionary.name}
                </h2>
                <h3>
                Description: {dictionary.description}
                </h3>
            </div>
            <AddWordPair
                setWordPairs={setWordPairs}
            />

            {

                wordPairs.map((wordpair) =>
                    <WordPair 
                        key={wordpair.id} 
                        {...wordpair}
                        setWordPairs={setWordPairs}
                    />
                )
                 
            }

        </div>
    )
}