import { Button } from "@material-ui/core";
import { useContext, useState } from "react";
import classes from './style/RegisterForm.module.css';
import UserService from '../services/user-service';
import { useHistory } from "react-router-dom";
import { Field, Form, Formik } from "formik";
import * as Yup from "yup";
import { StateContext } from "../App";


export default function RegisterForm(props) {
    const history = useHistory();
    const theme = useContext(StateContext);

    const [regData, setRegData] = useState(
        {
            username: '',
            password: '',
            passwordAgain: '',
        }
    );

    const signupSchema = Yup.object({
        username: Yup.string()
            .email()
            .required('Required'),
        password: Yup.string()
            .min(5)
            .max(20)
            .required('Required'),
        passwordAgain: Yup.string()
            .oneOf([Yup.ref('password'), null], 'Password does not match')
            .required('Required')
    });

    async function submit() {
        const result = await UserService.registerUser(regData);
        console.log(result);

        if (result.success) {
            history.push('/login');
        }
    }

    function handleChange(event) {
        const { value, id } = event.target;
        setRegData({
            ...regData, [id]: value
        });
    }

    return (
        <div 
            className={classes.register} 
            style={{
                background: theme.background,
                color: theme.foreground
                }}>
            <h1>Register Form</h1>
            <Formik
                initialValues={{
                    username: '',
                    password: '',
                    passwordAgain: '',
                }}
                validationSchema={signupSchema}
                onClick={() =>
                    handleChange
                }
            >
                {
                    (formik) => (
                        <Form>
                            <h3>Email:</h3>
                            <Field
                                id="username"
                                type="email"
                                name="username"
                                // onChange={handleChange}
                                label="Email"
                            />
                            {
                                formik.errors.username && formik.touched.username
                                    ?
                                    <div style={{ color: 'red' }}>{formik.errors.username}</div>
                                    :
                                    null
                            }
                            <h3>Password:</h3>
                            <Field
                                id="password"
                                type="password"
                                name="password"
                                // onChange={handleChange}
                                label="Password"
                            />
                            {
                                formik.errors.password && formik.touched.password
                                    ?
                                    <div style={{ color: 'red' }}>{formik.errors.password}</div>
                                    :
                                    null
                            }
                            <h3>Confirm Password:</h3>
                            <Field
                                id="passwordAgain"
                                type="password"
                                name="passwordAgain"
                                // onChange={handleChange}
                                label="Confirm Password"
                            />
                            {
                                formik.errors.passwordAgain && formik.touched.passwordAgain
                                    ?
                                    <div style={{ color: 'red' }}>{formik.errors.passwordAgain}</div>
                                    :
                                    null
                            }
                            <br />

                            <Button
                                type="submit"
                                onClick={submit}
                                size="large"
                                variant="outlined"
                            >
                                Register
                            </Button>
                        </Form>
                    )
                }
            </Formik>
        </div>
    );
}