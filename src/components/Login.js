import { useHistory } from "react-router-dom";
import style from './style/Login.module.css';
import {
    Button,
    TextField,
    FormControl,
    CircularProgress
} from "@material-ui/core";
import { useContext, useState } from "react";
import UserService from "../services/user-service";
import { StateContext } from "../App";

export default function Login() {
    const history = useHistory();
    const theme = useContext(StateContext);

    const [userData, setUserdata] = useState({
        username: '',
        password: '',
    });

    const [inputSent, setInputSent] = useState({
        handler: true,
    });

    function navigateToRegister() {
        history.push("/register");
    }

    async function handleButton() {
        setInputSent({
            handler: false,
        });

        const resp = await UserService.loginUser(userData);

        if (resp.success) {
            history.push('/mydict');
        }
    }

    async function handleEnter(event) {
        if(event.key === 'Enter') {
        setInputSent({
            handler: false,
        });

        const resp = await UserService.loginUser(userData);

        if (resp.success) {
            history.push('/mydict');
        }
    }
    }

    function handleChange( event ) {
        const { value, id } = event.target;
        setUserdata({ ...userData, [id]: value });
    }

    return (
        <div className={style.main}
        style={{
            color: theme.foreground,
                background: theme.background
        }}
        >
            <FormControl>
                <TextField 
                    id="username"
                    value={userData.username}
                    onChange={handleChange}
                    className={style.textField} 
                    label="Email" 
                    disabled={!inputSent.handler}>
                </TextField>
                <TextField 
                    id="password"
                    value={userData.password}
                    onChange={handleChange}
                    className={style.textField} 
                    type="password" 
                    disabled={!inputSent.handler} 
                    label="Password">
                </TextField>

                {
                    inputSent.handler 
                        ? 
                            <Button 
                                className={style.button} 
                                variant="outlined" 
                                size="large" 
                                onClick={handleButton}
                                onKeyPress={(event) => handleEnter(event)}
                                >
                                    Login
                            </Button>
                        :
                            <CircularProgress className={style.spinner} />
                }

            </FormControl>
            <p>or <Button color="inherit" variant="outlined" size="small" onClick={navigateToRegister}>Sign Up</Button> now</p>

        </div>

    )
}