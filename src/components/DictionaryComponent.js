import { Button, Modal } from "@material-ui/core";
import classes from "./style/DictionaryComponent.module.css";
import styles from './style/modalpopup.module.css';
import { useHistory } from 'react-router-dom';
import { useState } from 'react';

function rand() {
    return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
    const top = 30 + rand();
    const left = 30 + rand();

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
        backgroundColor: 'white',
        height: '200px',
        width: '350px',
        padding: '20px'
    };
}


export default function DictionaryComponent(props) {
    const history = useHistory();
    const [openModal, setOpenModal] = useState();
    const [modalStyle] = useState(getModalStyle());


    function openDict(id) {
        history.push('/mydict/' + id);
    }

    function deleteItem(id) {
        props.handleDelete(id);
    }

    function handleModal() {
        setOpenModal(true);
    }

    function handleModalClose() {
        setOpenModal(false);
    }

    return (

        < div
            key={props.id}
            className={classes.boxes}
            onMouseEnter={handleModal}
            onMouseLeave={handleModalClose}
        >

            <div
            >
                <p>{props.name}</p>
                <br /><br />
                <div
                    className={classes.container}

                >
                    <Button
                        className={classes.flexbutton}
                        variant="outlined"
                        onClick={() => openDict(props.id)}
                    >
                        Open
                    </Button>
                    <Button
                        className={classes.flexbutton}
                        variant="outlined"
                        color="secondary"
                        // onClick={props.handleDelete}
                        onClick={() => props.handleDelete(props.id)}
                        onMouseLeave={handleModalClose}
                    >
                        Delete
                    </Button>
                </div>
            </div>
            <Modal
                open={openModal}
                style={modalStyle}
            >
                <div className={styles.mymodal}>
                    <h2 id="modal-title">{props.name}</h2>
                    <p id="modal-description">{props.description}</p>
                </div>
            </Modal>
        </div >
    )
}