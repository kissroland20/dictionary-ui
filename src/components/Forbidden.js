import { Button } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import styles from "./style/Forbidden.module.css";


export default function Forbidden() {
    let history = useHistory();
    function  navigateBack() {
        history.push("/home"); 
    }

    function  navigateToRegister() {
        history.push("/register"); 
    }

    return (
        <div className={
            styles.container
        }>
            <div className={
                styles.forbidden

            }
            >
                <h1>Forbidden</h1>
                <div>Log in or 
                    <a 
                        className={styles.button} 
                        onClick={navigateToRegister}
                    >
                            , Sign Up
                    </a>
                    , if you want to get this content!
                </div>
                <Button 
                    variant="contained" 
                    color="primary" 
                    onClick={navigateBack}
                    style={{
                        margin: '10px'
                    }}
                >
                   Back to Home Page
                </Button>
            </div>




        </div>

    )
}