import { Button } from "@material-ui/core";
import UserService from "../services/user-service";
import classes from "./style/Login.module.css";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { useContext } from "react";
import { DispatchContext, StateContext } from "../App";
import Brightness3Icon from '@material-ui/icons/Brightness3';
import WbSunnyIcon from '@material-ui/icons/WbSunny';

export default function Profile() {
    const theme = useContext(StateContext);
    const dispatch = useContext(DispatchContext);

    const signupSchema = Yup.object({
        password: Yup.string()
            .min(5)
            .max(20)
            .required('Required'),
        newPassword: Yup.string()
            .min(5, 'Too short')
            .max(20)
            .required('Required'),
        confirmNewPassword: Yup.string()
            .oneOf([Yup.ref('newPassword'), null], 'Passwords must match')
            .required('Required'),
    });

    async function handleClick(values) {

        const a = await UserService.changePassword(values);
        console.log(a);
        if (a.success === true) {
            alert('password changed');
        }
    }

    return (
        <>
            <div
                className={classes.main}
                style={{
                    background: theme.background,
                    color: theme.foreground
                }}
            >
                <h1>Dark/light mode:</h1>

                {
                    theme.background === '#FFFFFF'
                        ?
                        <Button
                            color="inherit"
                            onClick={() => (
                                dispatch({ type: 'changeToDark' })
                            )}
                        >
                            <Brightness3Icon />
                        </Button>
                        :
                        <Button
                            color="inherit"
                            onClick={() => (
                                dispatch({ type: 'changeToLight' })
                            )}
                        >
                            <WbSunnyIcon />
                        </Button>
                }

                <h1>Password change:</h1>
                <Formik
                    initialValues={{
                        password: '',
                        newPassword: '',
                        confirmNewPassword: ''
                    }}
                    validationSchema={signupSchema}
                    onSubmit={values => handleClick(values)}
                >
                    {(formik) => (
                        <Form
                            className={classes.formikStyle}
                        >
                            <h4>Password:</h4>
                            <Field
                                name="password"
                                type="password"
                                label="Current Password"
                            />
                            {
                                formik.errors.password && formik.touched.password
                                    ?
                                    <div style={{ color: "red" }}>{formik.errors.password}</div>
                                    :
                                    null
                            }
                            <h4>New Password:</h4>
                            <Field
                                name="newPassword"
                                type="password"
                                label="New Password"
                            />
                            {
                                formik.errors.newPassword && formik.touched.newPassword
                                    ?
                                    <div style={{ color: "red" }}>{formik.errors.newPassword}</div>
                                    :
                                    null
                            }
                            <h4>Confirm New Password:</h4>
                            <Field
                                name="confirmNewPassword"
                                type="password"
                                label="New Password Again"
                            />
                            {
                                formik.errors.confirmNewPassword && formik.touched.confirmNewPassword
                                    ?
                                    <div style={{ color: "red" }}>{formik.errors.confirmNewPassword}</div>
                                    :
                                    null
                            }
                            <br />
                            <Button
                                type="submit"
                                variant="outlined"
                            >
                                Change password
                            </Button>
                        </Form>
                    )
                    }
                </Formik>
            </div>
        </>
    )
}