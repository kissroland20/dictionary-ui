import { Button, CircularProgress } from "@material-ui/core";
import styles from "./style/wordpair.module.css";
import DeleteIcon from "@material-ui/icons/Delete";
import { useState } from "react";
import WordPairService from "../services/wordpair-service";
import SheetService from "../services/sheet-service";
import { useLocation } from "react-router-dom";

export default function WordPair(props) {
    const [editContent, setEditContent] = useState(false);
    const [isPending, setIsPending] = useState(false);

    const location = useLocation();

    /*
    
        Hozz létre state változót isPending néven
        Mielőtt kimenne az update kérés, állítsd true-ra
        Listázás után állítsd false-ra
        Kirenderelt tartalmat módosítsd úgy, hogy vegye figyelmbe az isPending state változót
    
    */

    function handleClick() {
        setEditContent(true);
    }

    async function handleDelete(id) {
        const idFromURL = +(location.pathname.match(/[0-9]+/g));
        await WordPairService.deleteWordpair(id);
        const fetchedWordpairs =  await SheetService.retriveOneSheet(idFromURL);
        props.setWordPairs(fetchedWordpairs.wordPairs);
    }

    async function handleEnter(event) {
        const idFromURL = +(location.pathname.match(/[0-9]+/g));
        if (event.key === 'Enter') {
            setEditContent(false);
            if(event.target.previousSibling == null) {
                const userData = {
                    leftValue: event.target.textContent,
                    rightValue: event.target.nextSibling.textContent,
                    id: event.target.id
                }
                setIsPending(true);
               await WordPairService.updateWordpair(userData);
               const fetchedWordpairs =  await SheetService.retriveOneSheet(idFromURL);
               setIsPending(false);
               props.setWordPairs(fetchedWordpairs.wordPairs);
                
               
            } else {
                const userData = {
                    leftValue: event.target.previousSibling.textContent,
                    rightValue: event.target.textContent,
                    id: event.target.id
                }
                setIsPending(true);
                await WordPairService.updateWordpair(userData);
                const fetchedWordpairs =  await SheetService.retriveOneSheet(idFromURL);
                setIsPending(false);
                props.setWordPairs(fetchedWordpairs.wordPairs);
            }
        }
    }

    return (
        <div className={styles.wordpair}>
                {
                    isPending 
                    ?
                    <CircularProgress />
                    :
            <div className={styles.wordpair} >
                <h3
                    id={props.id}
                    contentEditable={editContent}
                    className={styles.single_word}
                    onClick={() => handleClick()}
                    onKeyPress={(event) => handleEnter(event)}
                >
                    {props.leftValue}
                </h3>
                <h3
                    id={props.id}
                    contentEditable={editContent}
                    className={styles.single_word}
                    onClick={() => handleClick()}
                    onKeyPress={(event) => handleEnter(event)}
                    >
                    {props.rightValue}
                </h3>
            </div>
                }
            <Button
                className={styles.button}
                variant="contained"
                size="small"
                color="secondary"
                startIcon={<DeleteIcon />}
                onClick={() => handleDelete(props.id)}
            >
                Delete
            </Button>
        </div>
    );
}