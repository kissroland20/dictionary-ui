import { TextField } from "@material-ui/core";
import classes from './style/practice.module.css';

export default function RightValue(props) {
    return (
        <>
            <TextField
                className={classes.wordStyle}
                id="outlined-secondary-2"
                variant="outlined"
                color="secondary"
                disabled
                value={
                    props.show 
                    ?
                    props.value
                    :
                    ''
                }          
            />
        </>
    )
}