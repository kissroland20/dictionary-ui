import { useContext, useEffect, useState } from 'react';
import classes from './style/MyDict.module.css';
import { Button, LinearProgress, Modal, Input } from '@material-ui/core';
import DictionaryComponent from './DictionaryComponent';
import SheetService from '../services/sheet-service.js';
import { Fragment } from 'react';
import { StateContext } from '../App';
import BasicPagination from './BasicPagination';

export default function MyDict() {
    const [dictionaries, setDictionary] = useState([]);
    const [newDict, setNewDict] = useState([]);
    const [isPending, setIsPending] = useState(false);
    const [openModal, setOpenModal] = useState();
    const theme = useContext(StateContext);

    useEffect(() => {
        async function loadData() {
            setIsPending(true);
            const fetchedDictionaries = await SheetService.retriveAllSheet();
            setDictionary(fetchedDictionaries);
            setIsPending(false);
        }
        loadData();
    }, []);

    async function deleteItem(id) {
        setIsPending(true);
        await SheetService.deleteSheet(id);
        const fetchedDictionaries = await SheetService.retriveAllSheet();
        setDictionary(fetchedDictionaries);
        setIsPending(false);
    }

    function handleChange(event) {
        const { value, name } = event.target;
        setNewDict(
            {
                ...newDict,
                [name]: value,
            },
        );
        console.log(event.target.value);
    }

    function handleModal() {
        setOpenModal(true);
    }

    function handleModalClose() {
        setOpenModal(false);
    }

    async function addNewDict(event) {
        setIsPending(true);
        await SheetService.addNewSheet(newDict);
        handleModalClose();
        const fetchedDictionaries = await SheetService.retriveAllSheet();
        setDictionary(fetchedDictionaries);
        setIsPending(false);
    }

    return (
        <Fragment>
            <div
                className={classes.container}
                style={{
                    color: theme.foreground,
                    background: theme.background
                }}
            >
                {/* <ModalAddNewDict open={openModal}/> */}
                <div className={classes.addNewElement} >
                    <Button
                        className={classes.addbutton}
                        variant="outlined"
                        color="primary"
                        size="large"
                        onClick={handleModal}
                    >
                        ADD NEW
                    </Button>

                    <BasicPagination {...dictionaries} />

                </div>
                {
                    isPending
                        ?
                        <LinearProgress className={classes.progressbar} />
                        :
                        dictionaries.map((item) =>
                            item.id ?
                                <DictionaryComponent
                                    key={item.id}
                                    {...item}
                                    handleDelete={(id) => deleteItem(id)}
                                />
                                :
                                ''
                        )
                }
                <Modal
                    open={openModal}
                    className={classes.modal}
                >
                    <div className={classes.modalStyle}>
                        <div
                            className={classes.modalInputs}>
                            <Input
                                // className={classes.addInputField}
                                type="text"
                                name="name"
                                placeholder="Add new dictionary"
                                onChange={(event) => handleChange(event)}
                            />
                            <Input
                                // className={classes.addInputField}
                                type="text"
                                name="description"
                                placeholder="Add description"
                                multiline
                                onChange={(event) => handleChange(event)}
                            />
                        </div>
                        <div
                            className={classes.modalButtons}>
                            <Button
                                onClick={addNewDict}
                                variant="outlined"
                            >Save
                            </Button>
                            <Button
                                variant="outlined"
                                onClick={handleModalClose}
                            >Close
                            </Button>
                        </div>
                    </div>
                </Modal>
            </div>
        </Fragment>
    )
}