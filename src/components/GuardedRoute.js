import { Route, Redirect } from "react-router";
import UserService from '../services/user-service.js';

export default function GuardedRoute(props) {
    console.log(props)
    return (
        <Route
            path={props.location.pathName}
        >
            {
                UserService.isUserLoggedIn()
                    ?
                    <props.component />
                    :
                    <Redirect to='/forbidden' />
            }
        </Route>

        // <Route path={props.path}>
        //     {
        //         UserService.isUserLoggedIn()
        //         ?
        //         props.children
        //         :
        //         <Redirect to='/forbidden' />
        //     }
        // </Route>
    );
}
