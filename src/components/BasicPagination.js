import { Pagination } from "@material-ui/lab";
import { useLocation } from "react-router-dom";
import classes from './style/MyDict.module.css';

export default function BasicPagination(props) {
    const location = useLocation();
    console.log(new URLSearchParams(location.search));
    return (
        <div className={classes.pageBar} >
            <Pagination count={2} color="primary" />
        </div>
    )
}